//
//  HuaState.swift
//  HuaRongdao
//
//  Created by biesx on 2019/1/17.
//  Copyright © 2019 biesx. All rights reserved.
//

import UIKit

//华容道的某个状态
struct HuaState: SXNodeState {
    static func dataToIdentifier(_ data: [String]) -> String {
        let temp = data.joined(separator: ",")
        return temp
    }
    
    static func identifierToData(_ iden: String) -> [String] {
        let temp = iden.components(separatedBy: ",")
        return temp
    }
    
    var parentState: SXNodeState?
    
    var data: [String]
    
    var identifier: String
    
    func childeStates() -> [SXNodeState] {
        return checkMove(state: data)
    }
    
    init(parentState: SXNodeState?, data: [String]) {
        self.parentState = parentState
        self.data = data
        self.identifier = data.joined(separator: ",")
    }
    
    init(identifier: String) {
        let temp = identifier.components(separatedBy: ",")
        self.init(parentState: nil, data: temp)
    }
    

    // MARK: private
    
    func checkMove(state: [String]) -> [HuaState] {
        var states = [HuaState]()
        
        let space0 = state.lastIndex(of: "01")!
        let result0 = chechOneSpace(space: space0, state: state)
        states += result0
        
        let space1 = state.lastIndex(of: "02")!
        let result1 = chechOneSpace(space: space1, state: state)
        states += result1
        
        let result3 = chechTwoSpace(space0: space0, space1: space1, state: state)
        states += result3
        
//        print( "-----\(space0)-----\(space1)")
        return states
    }
    
    
    
    func checkWall(index: Int) -> [Int] {
        switch index {
        case 0:
            return [1, 4]
        case 1, 2:
            return [1, 4, -1]
        case 3:
            return [4, -1]
        case 7, 11, 15:
            return [4, -1, -4]
        case 19:
            return [-1, -4]
        case 17, 18:
            return [1, -1, -4]
        case 16:
            return [1, -4]
        case 4, 8, 12:
            return [1, 4, -4]
        default:
            return [1, 4, -1, -4]
        }
    }
    
    func chechTwoSpace(space0: Int, space1: Int, state: [String]) -> [HuaState] {
        let absSpace = abs(space0 - space1)
        guard absSpace == 1 || absSpace == 4 else {
            return []
        }
        
        var newStates = [HuaState]()
        for add in checkWall(index: space0) {
            let index = space0 + add
            //位置的数字
            let value = state[index]
            //所有的位置
            var indexs = [Int]()
            state.enumerated().forEach { (index, item) in
                if item == value {
                    indexs.append(index)
                }
            }
            if indexs.count == 1 {
                continue
            }
            var newState = state
            newState.replace(space: [space0, space1], index: add, indexs: indexs)
            let temp = HuaState.init(parentState: self, data: newState)
            newStates.append(temp)
        }
        return newStates
    }
    
    func chechOneSpace(space: Int, state: [String]) -> [HuaState] {
        var newStates = [HuaState]()
        for add in checkWall(index: space) {
            let index = space + add
            //位置的数字
            let value = state[index]
            //所有的位置
            var indexs = [Int]()
            state.enumerated().forEach { (index, item) in
                if item == value {
                    indexs.append(index)
                }
            }
            var newState = state
            let count = indexs.count
            if count == 1 {
                newState.replace(space: [space], index: add, indexs: indexs)
                let temp = HuaState.init(parentState: self, data: newState)
                newStates.append(temp)
            } else if count == 2 && abs(add) == abs(indexs[0] - indexs[1]) {
                newState.replace(space: [space], index: add, indexs: indexs)
                let temp = HuaState.init(parentState: self, data: newState)
                newStates.append(temp)
            }
        }
        return newStates
    }
    
    // 空 空 / 曹 / 关羽 黄忠 马云 张飞 赵云 / 卒 卒 卒 卒 / 卒 卒 卒 卒
    // space / cao / guan huang ma zhang zhao / zu / zu
    //1  2 / 3 / 4  5  6  7  8 / 9  a  b  c _ d  e  f  g
    func identifier(states: [Int]) -> String {
        var temp = ""
        states.forEach { (item) in
            switch item {
            case 0 ... 9:
                temp.append("\(item)")
            case 10:
                temp.append("a")
            case 11:
                temp.append("b")
            case 12:
                temp.append("c")
            case 13:
                temp.append("d")
            case 14:
                temp.append("e")
            case 15:
                temp.append("f")
            case 16:
                temp.append("g")
            case 17:
                temp.append("h")
            case 18:
                temp.append("i")
            case 19:
                temp.append("j")
            default:
                break
            }
        }
        return temp
    }
    
    static func states(identifier: String) -> [Int] {
        var arr = [Int]()
        
        identifier.forEach { (char) in
            switch char {
            case "0":
                arr.append(0)
            case "1":
                arr.append(1)
            case "2":
                arr.append(2)
            case "3":
                arr.append(3)
            case "4":
                arr.append(4)
            case "5":
                arr.append(5)
            case "6":
                arr.append(6)
            case "7":
                arr.append(7)
            case "8":
                arr.append(8)
            case "9":
                arr.append(9)
            case "a":
                arr.append(10)
            case "b":
                arr.append(11)
            case "c":
                arr.append(12)
            case "d":
                arr.append(13)
            case "e":
                arr.append(14)
            case "f":
                arr.append(15)
            case "g":
                arr.append(16)
            case "h":
                arr.append(17)
            case "i":
                arr.append(18)
            case "j":
                arr.append(19)
            default:
                break
            }
        }
        return arr
    }
}

extension Array {
    mutating func replace(space: [Int], index: Int, indexs: [Int]) {
        guard space.count == 2 else {
            let replaceIndex = index > 0 ? indexs.last! : indexs.first!
            let to = self[replaceIndex]
            self[replaceIndex] = self[space.first!]
            self[space.first!] = to
            return
        }
        
        let space0 = space[0]
        let space1 = space[1]
        
        let replaceIndex0: Int
        let replaceIndex1: Int
        if indexs.count == 4 {
            switch index {
            case 1:
                replaceIndex0 = indexs[1]
                replaceIndex1 = indexs[3]
            case -1:
                replaceIndex0 = indexs[0]
                replaceIndex1 = indexs[2]
            case 4:
                replaceIndex0 = indexs[2]
                replaceIndex1 = indexs[3]
            case -4:
                replaceIndex0 = indexs[0]
                replaceIndex1 = indexs[1]
            default:
                return
            }
        } else {
            replaceIndex0 = indexs[0]
            replaceIndex1 = indexs[1]
        }
        
        let to0 = self[replaceIndex0]
        let to1 = self[replaceIndex1]
        self[replaceIndex0] = self[space0]
        self[replaceIndex1] = self[space1]
        self[space0] = to0
        self[space1] = to1
        
    }
}
