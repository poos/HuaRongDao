//
//  SXProtocal.swift
//  HuaRongdao
//
//  Created by biesx on 2019/1/18.
//  Copyright © 2019 biesx. All rights reserved.
//

import UIKit

//某个状态
protocol SXNodeState {
    var parentState: SXNodeState? { get set }
    
    var data: [String] { get set }
    
    var identifier: String { get set }
    
    func childeStates() -> [SXNodeState]
}

//某个状态的权重 暂未使用
protocol SXAStarState {
    var fromValue: Int { get set }
    
    var toValue: Int { get set }
    
    var totalValue: Int { get set }
    
    func toTargetStatus(_ from: SXNodeState) -> Int
}

//搜索器
protocol SXSeacher {
    var startState: SXNodeState { get set }
    
    var targetState: SXNodeState { get set }
    
    var successComparator: (SXNodeState, SXNodeState) -> Bool { get set }
    
    func search() -> Bool
    
    func pathWithState() -> [SXNodeState]
}
