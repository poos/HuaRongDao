//
//  HuaSearch.swift
//  HuaRongdao
//
//  Created by Shown on 2019/1/18.
//  Copyright © 2019 biesx. All rights reserved.
//

import UIKit


class HuaSearch: SXSeacher {
    var startState: SXNodeState
    
    var targetState: SXNodeState
    
    var successComparator: (SXNodeState, SXNodeState) -> Bool
    
    init(start: SXNodeState, target: SXNodeState) {
        startState = start
        targetState = target
        successComparator = { (node0, node1) in
            let iden0 = node0.identifier
            let iden1 = node1.identifier
            guard iden0 != iden1 else {
                return true
            }
            
            var bossIndexs = [Int]()
            iden1.enumerated().forEach({ (index, item) in
                if item == "3" {
                    bossIndexs.append(index)
                }
            })
            var trueIndex = 0
            iden0.enumerated().forEach({ (index, item) in
                if bossIndexs.contains(index) && item == "3" {
                    trueIndex += 1
                }
            })
            //            print("------\n\(iden0)\n\(iden1)\n--------------------")
            
            return trueIndex == 4
        }
    }
    
    func search() -> Bool {
        historyIdentifier.insert(startState.identifier)
        let child = getChild(state: startState)
        //        child.map({ $0.identifier }).forEach { (iden) in
        //            historyIdentifier.insert(iden)
        //        }
        nextStates += child
        
        
        
        while nextStates.count > 0 {
            let first = nextStates.first!
            nextStates.remove(at: 0)
            historyIdentifier.insert(first.identifier)
            
            if successComparator(first, targetState) {
                print("--------success------------查询节点---\(historyIdentifier.count)---------记录节点---\(nextStates.count)----")
                succcesState = first
                return true
            }
            let child = getChild(state: first)
            
            let selectChild = child.filter { (state) -> Bool in
                return !historyIdentifier.contains(state.identifier)
            }
            nextStates += selectChild
            
            
            let max = 1000000
            let logCount = nextStates.count
            if logCount % Int(sqrt(Double(max))) == 0 {
                print("----当前查询个数----\(historyIdentifier.count)-------已经存储的节点数----\(nextStates.count)-----")
            }
            
            if historyIdentifier.count > max {
                print("#############    查询已经超过 \(max)    ###############")
                break
            }
        }
        return succcesState != nil
    }
    
    func pathWithState() -> [SXNodeState] {
        guard succcesState != nil else { return [] }
        
        var paths = [SXNodeState]()
        var last = succcesState!
        while last.parentState != nil {
            paths.append(last)
            last = last.parentState!
        }
        return paths.reversed()
    }
    
    //MARK: private
    
    private var nextStates = [SXNodeState]()
    
    private var historyIdentifier = Set<String>()
    
    private var succcesState: SXNodeState?
    
    private func getChild(state: SXNodeState) -> [SXNodeState] {
        let child = state.childeStates()
        return child
    }
    
}

