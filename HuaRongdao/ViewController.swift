//
//  ViewController.swift
//  HuaRongdao
//
//  Created by biesx on 2019/1/17.
//  Copyright © 2019 biesx. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 空1 空2 / 曹3 / 关羽4 黄忠5 马超6 张飞7 赵云8 / 卒9 卒a 卒b 卒c / 卒d 卒e 卒f 卒g
        // space / cao / guan huang ma zhang zhao / zu / zu
        //1  2 / 3 / 4  5  6  7  8 / 9  a  b  c _ d  e  f  g
        
        test()
        
        let tagG = UITapGestureRecognizer.init(target: self, action: #selector(test))
        view.addGestureRecognizer(tagG)
    }
    
    
    @objc func test() {
        
//        let fromIdentifier = "9337a337b448c5581662"
//        let fromIdentifier = "449a127833783366bc55"
        
//        let fromIdentifier = "9a78447833123366bc55"
        let fromIdentifier = "09,10,07,08,04,04,07,08,03,03,06,06,03,03,05,05,11,12,01,02"
//        let fromIdentifier = "9a7844783366335512bc"
//        let fromIdentifier = "9a7844786612335533bc"
        let from = HuaState.init(identifier: fromIdentifier)
        
        let toIdentifier = "00,00,00,00,00,00,00,00,00,00,00,00,00,03,03,00,00,03,03,00"
        let to = HuaState.init(identifier: toIdentifier)
        
        let search = HuaSearch.init(start: from, target: to)
        
        let date = Date().timeIntervalSince1970
        guard search.search() else {
            print("----------fiald----!----------")
            return
        }
        let date2 = Date().timeIntervalSince1970
        
        print("----------success----!----用时----\(date2 - date)----s--------------")
        printState(from.data)
        let paths = search.pathWithState()
        paths.forEach { (node) in
            printState(node.data)
        }
        
        print("----求解步数----\(paths.count)----!----------")
    }

    private func printState(_ identifier: [String]) {
        var temp = ""
        var indexs = 0
        identifier.forEach { (char) in
            switch char {
            case "01", "02":
                temp.append("〇")
            case "03":
                temp.append("曹")
            case "04":
                temp.append("关")
            case "05":
                temp.append("黄")
            case "06":
                temp.append("马")
            case "07":
                temp.append("张")
            case "08":
                temp.append("赵")
            default:
                temp.append("卒")
            }
            indexs += 1
            if indexs % 4 == 0 {
                temp.append("\n")
            }
        }
        print(temp)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        print("####################   didReceiveMemoryWarning    ###########################")
    }
}

